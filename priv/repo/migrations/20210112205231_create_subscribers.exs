defmodule NotifyEvents.Repo.Migrations.CreateSubscribers do
  use Ecto.Migration

  def change do
    create table(:subscribers) do
      add :name, :string
      add :pn_endpoint, :string
      add :pn_p256dh, :string
      add :pn_auth, :string
      add :ip_addr, :string
      add :user_agent, :string

      timestamps()
    end

  end
end
