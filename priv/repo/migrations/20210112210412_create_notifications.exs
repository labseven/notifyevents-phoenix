defmodule NotifyEvents.Repo.Migrations.CreateNotifications do
  use Ecto.Migration

  def change do
    create table(:notifications) do
      add :title, :string
      add :body, :text

      timestamps()
    end

  end
end
