defmodule NotifyEvents.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :title, :string
      add :body, :text
      add :start_date, :naive_datetime

      timestamps()
    end

  end
end
