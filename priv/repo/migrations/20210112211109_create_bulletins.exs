defmodule NotifyEvents.Repo.Migrations.CreateBulletins do
  use Ecto.Migration

  def change do
    create table(:bulletins) do
      add :title, :string

      timestamps()
    end

  end
end
