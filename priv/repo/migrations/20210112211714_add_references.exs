defmodule NotifyEvents.Repo.Migrations.AddReferences do
  use Ecto.Migration

  def change do
    alter table(:subscribers) do
      add :bulletin_id,  references(:bulletins, on_delete: :delete_all), null: false
    end

    alter table(:notifications) do
      add :bulletin_id,  references(:bulletins, on_delete: :delete_all), null: false
    end

    alter table(:events) do
      add :bulletin_id,  references(:bulletins, on_delete: :delete_all), null: false
    end

    create index(:events, [:bulletin_id])
    create index(:notifications, [:bulletin_id])
    create index(:subscribers, [:bulletin_id])


    alter table(:bulletins) do
      add :user_id,  references(:users, on_delete: :delete_all), null: false
    end

    create index(:bulletins, [:user_id])

  end
end
