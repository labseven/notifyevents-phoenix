# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :notify_events,
  ecto_repos: [NotifyEvents.Repo]

# Configures the endpoint
config :notify_events, NotifyEventsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: NotifyEventsWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: NotifyEvents.PubSub,
  live_view: [signing_salt: "47KEP2oQ"]

config :web_push_encryption, :vapid_details,
  subject: "mailto:administrator@example.com",
  public_key: System.get_env("WEB_PUSH_PUBLIC"),
  private_key: System.get_env("WEB_PUSH_PRIVATE")

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
