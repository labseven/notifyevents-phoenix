defmodule NotifyEvents.BoardTest do
  use NotifyEvents.DataCase

  alias NotifyEvents.Board

  describe "subscribers" do
    alias NotifyEvents.Board.Subscriber

    @valid_attrs %{ip_addr: "some ip_addr", name: "some name", pn_auth: "some pn_auth", pn_endpoint: "some pn_endpoint", pn_p256dh: "some pn_p256dh", user_agent: "some user_agent"}
    @update_attrs %{ip_addr: "some updated ip_addr", name: "some updated name", pn_auth: "some updated pn_auth", pn_endpoint: "some updated pn_endpoint", pn_p256dh: "some updated pn_p256dh", user_agent: "some updated user_agent"}
    @invalid_attrs %{ip_addr: nil, name: nil, pn_auth: nil, pn_endpoint: nil, pn_p256dh: nil, user_agent: nil}

    def subscriber_fixture(attrs \\ %{}) do
      {:ok, subscriber} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Board.create_subscriber()

      subscriber
    end

    test "list_subscribers/0 returns all subscribers" do
      subscriber = subscriber_fixture()
      assert Board.list_subscribers() == [subscriber]
    end

    test "get_subscriber!/1 returns the subscriber with given id" do
      subscriber = subscriber_fixture()
      assert Board.get_subscriber!(subscriber.id) == subscriber
    end

    test "create_subscriber/1 with valid data creates a subscriber" do
      assert {:ok, %Subscriber{} = subscriber} = Board.create_subscriber(@valid_attrs)
      assert subscriber.ip_addr == "some ip_addr"
      assert subscriber.name == "some name"
      assert subscriber.pn_auth == "some pn_auth"
      assert subscriber.pn_endpoint == "some pn_endpoint"
      assert subscriber.pn_p256dh == "some pn_p256dh"
      assert subscriber.user_agent == "some user_agent"
    end

    test "create_subscriber/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Board.create_subscriber(@invalid_attrs)
    end

    test "update_subscriber/2 with valid data updates the subscriber" do
      subscriber = subscriber_fixture()
      assert {:ok, %Subscriber{} = subscriber} = Board.update_subscriber(subscriber, @update_attrs)
      assert subscriber.ip_addr == "some updated ip_addr"
      assert subscriber.name == "some updated name"
      assert subscriber.pn_auth == "some updated pn_auth"
      assert subscriber.pn_endpoint == "some updated pn_endpoint"
      assert subscriber.pn_p256dh == "some updated pn_p256dh"
      assert subscriber.user_agent == "some updated user_agent"
    end

    test "update_subscriber/2 with invalid data returns error changeset" do
      subscriber = subscriber_fixture()
      assert {:error, %Ecto.Changeset{}} = Board.update_subscriber(subscriber, @invalid_attrs)
      assert subscriber == Board.get_subscriber!(subscriber.id)
    end

    test "delete_subscriber/1 deletes the subscriber" do
      subscriber = subscriber_fixture()
      assert {:ok, %Subscriber{}} = Board.delete_subscriber(subscriber)
      assert_raise Ecto.NoResultsError, fn -> Board.get_subscriber!(subscriber.id) end
    end

    test "change_subscriber/1 returns a subscriber changeset" do
      subscriber = subscriber_fixture()
      assert %Ecto.Changeset{} = Board.change_subscriber(subscriber)
    end
  end

  describe "events" do
    alias NotifyEvents.Board.Event

    @valid_attrs %{body: "some body", start_date: ~N[2010-04-17 14:00:00], title: "some title"}
    @update_attrs %{body: "some updated body", start_date: ~N[2011-05-18 15:01:01], title: "some updated title"}
    @invalid_attrs %{body: nil, start_date: nil, title: nil}

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Board.create_event()

      event
    end

    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Board.list_events() == [event]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Board.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Board.create_event(@valid_attrs)
      assert event.body == "some body"
      assert event.start_date == ~N[2010-04-17 14:00:00]
      assert event.title == "some title"
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Board.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, %Event{} = event} = Board.update_event(event, @update_attrs)
      assert event.body == "some updated body"
      assert event.start_date == ~N[2011-05-18 15:01:01]
      assert event.title == "some updated title"
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Board.update_event(event, @invalid_attrs)
      assert event == Board.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Board.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Board.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Board.change_event(event)
    end
  end

  describe "notifications" do
    alias NotifyEvents.Board.Notification

    @valid_attrs %{body: "some body", title: "some title"}
    @update_attrs %{body: "some updated body", title: "some updated title"}
    @invalid_attrs %{body: nil, title: nil}

    def notification_fixture(attrs \\ %{}) do
      {:ok, notification} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Board.create_notification()

      notification
    end

    test "list_notifications/0 returns all notifications" do
      notification = notification_fixture()
      assert Board.list_notifications() == [notification]
    end

    test "get_notification!/1 returns the notification with given id" do
      notification = notification_fixture()
      assert Board.get_notification!(notification.id) == notification
    end

    test "create_notification/1 with valid data creates a notification" do
      assert {:ok, %Notification{} = notification} = Board.create_notification(@valid_attrs)
      assert notification.body == "some body"
      assert notification.title == "some title"
    end

    test "create_notification/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Board.create_notification(@invalid_attrs)
    end

    test "update_notification/2 with valid data updates the notification" do
      notification = notification_fixture()
      assert {:ok, %Notification{} = notification} = Board.update_notification(notification, @update_attrs)
      assert notification.body == "some updated body"
      assert notification.title == "some updated title"
    end

    test "update_notification/2 with invalid data returns error changeset" do
      notification = notification_fixture()
      assert {:error, %Ecto.Changeset{}} = Board.update_notification(notification, @invalid_attrs)
      assert notification == Board.get_notification!(notification.id)
    end

    test "delete_notification/1 deletes the notification" do
      notification = notification_fixture()
      assert {:ok, %Notification{}} = Board.delete_notification(notification)
      assert_raise Ecto.NoResultsError, fn -> Board.get_notification!(notification.id) end
    end

    test "change_notification/1 returns a notification changeset" do
      notification = notification_fixture()
      assert %Ecto.Changeset{} = Board.change_notification(notification)
    end
  end

  describe "bulletins" do
    alias NotifyEvents.Board.Bulletin

    @valid_attrs %{title: "some title"}
    @update_attrs %{title: "some updated title"}
    @invalid_attrs %{title: nil}

    def bulletin_fixture(attrs \\ %{}) do
      {:ok, bulletin} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Board.create_bulletin()

      bulletin
    end

    test "list_bulletins/0 returns all bulletins" do
      bulletin = bulletin_fixture()
      assert Board.list_bulletins() == [bulletin]
    end

    test "get_bulletin!/1 returns the bulletin with given id" do
      bulletin = bulletin_fixture()
      assert Board.get_bulletin!(bulletin.id) == bulletin
    end

    test "create_bulletin/1 with valid data creates a bulletin" do
      assert {:ok, %Bulletin{} = bulletin} = Board.create_bulletin(@valid_attrs)
      assert bulletin.title == "some title"
    end

    test "create_bulletin/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Board.create_bulletin(@invalid_attrs)
    end

    test "update_bulletin/2 with valid data updates the bulletin" do
      bulletin = bulletin_fixture()
      assert {:ok, %Bulletin{} = bulletin} = Board.update_bulletin(bulletin, @update_attrs)
      assert bulletin.title == "some updated title"
    end

    test "update_bulletin/2 with invalid data returns error changeset" do
      bulletin = bulletin_fixture()
      assert {:error, %Ecto.Changeset{}} = Board.update_bulletin(bulletin, @invalid_attrs)
      assert bulletin == Board.get_bulletin!(bulletin.id)
    end

    test "delete_bulletin/1 deletes the bulletin" do
      bulletin = bulletin_fixture()
      assert {:ok, %Bulletin{}} = Board.delete_bulletin(bulletin)
      assert_raise Ecto.NoResultsError, fn -> Board.get_bulletin!(bulletin.id) end
    end

    test "change_bulletin/1 returns a bulletin changeset" do
      bulletin = bulletin_fixture()
      assert %Ecto.Changeset{} = Board.change_bulletin(bulletin)
    end
  end
end
