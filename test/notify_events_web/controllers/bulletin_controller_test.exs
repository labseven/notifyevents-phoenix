defmodule NotifyEventsWeb.BulletinControllerTest do
  use NotifyEventsWeb.ConnCase

  alias NotifyEvents.Board

  @create_attrs %{title: "some title"}
  @update_attrs %{title: "some updated title"}
  @invalid_attrs %{title: nil}

  def fixture(:bulletin) do
    {:ok, bulletin} = Board.create_bulletin(@create_attrs)
    bulletin
  end

  describe "index" do
    test "lists all bulletins", %{conn: conn} do
      conn = get(conn, Routes.bulletin_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Bulletins"
    end
  end

  describe "new bulletin" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.bulletin_path(conn, :new))
      assert html_response(conn, 200) =~ "New Bulletin"
    end
  end

  describe "create bulletin" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.bulletin_path(conn, :create), bulletin: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.bulletin_path(conn, :show, id)

      conn = get(conn, Routes.bulletin_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Bulletin"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.bulletin_path(conn, :create), bulletin: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Bulletin"
    end
  end

  describe "edit bulletin" do
    setup [:create_bulletin]

    test "renders form for editing chosen bulletin", %{conn: conn, bulletin: bulletin} do
      conn = get(conn, Routes.bulletin_path(conn, :edit, bulletin))
      assert html_response(conn, 200) =~ "Edit Bulletin"
    end
  end

  describe "update bulletin" do
    setup [:create_bulletin]

    test "redirects when data is valid", %{conn: conn, bulletin: bulletin} do
      conn = put(conn, Routes.bulletin_path(conn, :update, bulletin), bulletin: @update_attrs)
      assert redirected_to(conn) == Routes.bulletin_path(conn, :show, bulletin)

      conn = get(conn, Routes.bulletin_path(conn, :show, bulletin))
      assert html_response(conn, 200) =~ "some updated title"
    end

    test "renders errors when data is invalid", %{conn: conn, bulletin: bulletin} do
      conn = put(conn, Routes.bulletin_path(conn, :update, bulletin), bulletin: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Bulletin"
    end
  end

  describe "delete bulletin" do
    setup [:create_bulletin]

    test "deletes chosen bulletin", %{conn: conn, bulletin: bulletin} do
      conn = delete(conn, Routes.bulletin_path(conn, :delete, bulletin))
      assert redirected_to(conn) == Routes.bulletin_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.bulletin_path(conn, :show, bulletin))
      end
    end
  end

  defp create_bulletin(_) do
    bulletin = fixture(:bulletin)
    %{bulletin: bulletin}
  end
end
