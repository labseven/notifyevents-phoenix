// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import '../css/app.css'

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import deps with the dep name or local files with a relative path, for
// example:
//
//     import {Socket} from "phoenix"
//     import socket from "./socket"
//
import 'phoenix_html'


// Register service worker
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js')
      .then(function(sw) {
        // console.log("Service Worker Registered"); console.log(sw);

        var subscriptions =
            JSON.parse(window.localStorage.getItem('subscriptions'))
        if (!subscriptions) {
          window.localStorage.setItem(
              'subscriptions', JSON.stringify({bulletins: []}))
        }
        subscriptions = JSON.parse(window.localStorage.getItem('subscriptions'))
        // console.log("subscriptions: ", subscriptions);

        const btn = document.getElementById('sub_btn');
        if (btn) {
          btn.addEventListener('click', sub_btn_click);
          sw.pushManager.getSubscription().then(
              (subscription) => {
                  // console.log(subscription);
                  sub_btn_set(
                      !storage_is_subbed() ? 'Subscribe' : 'Unsubscribe')})
        }
      })
      .catch(function(error) {
        console.log('Service worker registration failed:', error);
      });
  ;
} else {
  sub_btn_set('can\'t sub 😰')
}


function sub_btn_set(text) {
  const btn = document.getElementById('sub_btn');
  btn.innerText = text
}

function sub_btn_click() {
  window.navigator.serviceWorker.getRegistration().then((sw) => {
    if (storage_is_subbed()) {
      push_unsubscribe(sw)
    } else {
      push_subscribe(sw)
    }
  })
}

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

function push_subscribe(serviceWorker) {
  sub_btn_set('Subscribing...')

  Notification.requestPermission().then(
      () => {// console.log('done requestPermission')
             serviceWorker.pushManager
                 .subscribe({
                   userVisibleOnly: true,
                   applicationServerKey: urlB64ToUint8Array(pn_serverkey)
                 })
                 .then((subscription) => {
                   subscription = subscription.toJSON()

                   var sub_data = {
                     pn_endpoint: subscription.endpoint,
                     pn_auth: subscription.keys.auth,
                     pn_p256dh: subscription.keys.p256dh,
                     bulletin_id: bulletin_id
                   }

                   var xhr = new XMLHttpRequest();
                   xhr.open('POST', '/api/subscribers')
                   xhr.setRequestHeader('content-type', 'application/json');
                   xhr.setRequestHeader('x-csrf-token', csrf_token)
                   xhr.send(JSON.stringify({subscriber: sub_data}))

                   xhr.onload = () => {
                     if (xhr.status == 200) {
                       // console.log("subscription success")
                       // console.log(subscription);
                       storage_add_sub()
                       sub_btn_set('Subscribed!')
                     } else {
                       throw 'bad response'
                     }
                   } xhr.ontimeout = () => {
                     console.error('subscribe timeout')
                     sub_btn_set('error 😥')
                   };

                   return subscription
                 })
                 .catch((err) => {
                   console.log('subscription error', err);
                   if (err.name == 'NotAllowedError') {
                     sub_btn_set('notifications blocked 😥')
                   } else {
                     sub_btn_set('error 😥')
                   }
                 })});
}

function push_unsubscribe(serviceWorker) {
  sub_btn_set('working on it...')
  storage_del_sub();

  if (serviceWorker) {
    serviceWorker.pushManager.getSubscription()
        .then((subscription) => {
          var xhr = new XMLHttpRequest();
          xhr.open('DELETE', '/api/subscribers')
          xhr.setRequestHeader('content-type', 'application/json');
          xhr.setRequestHeader('x-csrf-token', csrf_token)
          xhr.send(JSON.stringify({
            subscriber:
                {endpoint: subscription.endpoint, bulletin_id: bulletin_id}
          }))

          xhr.onload = () => {
            if (xhr.status == 200) {
              // console.log("unsubscribed");
              sub_btn_set('Unsubbed!')
            }
          } xhr.ontimeout = () => {
            console.error('unsubscribe timeout')
            sub_btn_set('error 😥')
          };
        })
        .catch((err) => {
          // console.log("subscription error", err);
          if (err.name == 'NotAllowedError') {
            sub_btn_set('notifications blocked 😥')
          } else {
            sub_btn_set('error 😥')
          }
        })
  }
}

function storage_is_subbed() {
  var subscriptions = JSON.parse(window.localStorage.getItem('subscriptions'))
  return subscriptions.bulletins.includes(bulletin_id)
}

function storage_add_sub() {
  var subscriptions = JSON.parse(window.localStorage.getItem('subscriptions'))
  subscriptions.bulletins.push(bulletin_id)
  // console.log(subscriptions);
  window.localStorage.setItem('subscriptions', JSON.stringify(subscriptions))
  return subscriptions
}

function storage_del_sub() {
  var subscriptions = JSON.parse(window.localStorage.getItem('subscriptions'))
  subscriptions.bulletins =
      subscriptions.bulletins.filter((x) => x != bulletin_id)
  // console.log(subscriptions);
  window.localStorage.setItem('subscriptions', JSON.stringify(subscriptions))
  return subscriptions
}