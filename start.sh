#!/bin/sh
# Adapted from Alex Kleissner's post, Running a Phoenix 1.3 project with docker-compose
# https://medium.com/@hex337/running-a-phoenix-1-3-project-with-docker-compose-d82ab55e43cf

set -e

echo "MIX_ENV is $MIX_ENV"

# Ensure the app's dependencies are installed
mix local.hex --force
mix deps.get

# Install JS libraries
echo "\nInstalling JS..."
cd assets && npm install
cd ..


# echo "Checking postgres ($DB_HOST:$DB_USER)"
# # Wait for Postgres to become available.
# # until psql -h $DB_HOST -U $DB_USER -c '\q' 2>/dev/null; do
# until psql -h db -U "postgres" -c '\q' 2>/dev/null; do

#   >&2 echo "Postgres is unavailable - sleeping"
#   sleep 1
# done

# echo "\nPostgres is available: continuing with database setup..."

# Potentially Set up the database
mix ecto.create
mix ecto.migrate

# echo "\nTesting the installation..."
# # "Prove" that install was successful by running the tests
# mix test

# Update digest for prod
if [ $MIX_ENV = "prod" ]; then
    echo "\ndigesting static files..."
    mix phx.digest
fi

echo "\n Launching Phoenix web server..."
# Start the phoenix web server
mix phx.server