# NotifyEvents

An app for posting about upcoming events and sending push notifications to browser clients. Live at [events.verynice.party](https://events.verynice.party/).

### Running in Docker
Create `.env` file from `env.example` filling in proper values, then run `docker-compose up`. You can visit [`localhost:4000`](http://localhost:4000). You can connect to the container with `docker exec -it notify_events_phoenix_web_1 bash` to run mix commands.


# Todo
* app.css is 3.3MB, trim that down to something reasonable ([tailwind docs](https://tailwindcss.com/docs/optimizing-for-production#removing-unused-css)) (rest of website is 33KB)

* add admin role with full access
  * admin panel for moderating site-wide content + user sign-ups
* put_assoc in changesets (instead of validate_required(:user_id))
* fix joins in getting bulletins (subscriber count in sql)

* Layout/UX
  * Improve text on Sign In/Register
    * Combine them into single form?

* Boards
  * Admin Options
    * unlisted (or even password protected?)
    * custom URL slugs
    * custom color theme
  * Rich text event description (with md?) ([earmark](https://github.com/pragdave/earmark) md processor + [html sanitizer](https://github.com/elixirstatus/phoenix_html_sanitizer))
  * Use Live-view for broadcasting updates to events

* Sort boards by subscriber count / featured / quickest growing

* Add multiple users per board, with permissions system (owner, moderator, editor ...)
* Create global admin panel for moderating content, sign-ups, etc

* Create REST API and reference PWA (with React?)