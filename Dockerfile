FROM elixir:latest

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update
RUN apt-get -y install nodejs build-essential inotify-tools postgresql-client
# RUN npm install -g npm@latest # WARNING: npm@7.6.3 (node@v14.16.0) fails to install required dependencies!
RUN node -v && npm -v


RUN mix local.hex --force
RUN mix local.rebar --force

WORKDIR /app

EXPOSE 4000