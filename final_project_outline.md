# Notify Events

Notify Events is a bulletin board for broadcasting event details through PWA push-notifications.

It was originally built for Adam's [coffee events](https://coffee.verynice.party/). I have rewritten this application a few times already, as a way of trying new technologies (React + Node, Vue + Rails, Full-stack Rails). Each time, I have added features and improved the tooling around deployment. This iteration, I am building it on Elixir with Phoenix, and adding user-signups so that anyone can create and publish to an events board.

I have started working on this project for WebDev in March, and have almost finished functional MVP. There are a few features and lots of polishing to be done, and want to dedicate my final project to this to have a webapp that I am proud of.

### Todo

* Finish push-notification subscription story
* Add good looking CSS (inspired by brutalist text-only websites)
* Rebrand with a better name. But 'notify' already ends in -fy so I'm all out of ideas.
* ~~Add users that can create and manage boards~~
    * ~~Sign-on with email magic-link~~
    * Add multiple users per board, with permissions system (owner, moderator, editor ...)
* Create global admin panel for moderating content, sign-ups, etc

More details can be found in the [readme](https://gitlab.com/labseven/notifyevents-phoenix).

### Members
Adam Novotny