defmodule NotifyEventsWeb.Router do
  use NotifyEventsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :browser_api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :with_session do
    plug NotifyEvents.Accounts.Session
  end

  scope "/", NotifyEventsWeb do
    pipe_through :browser
    pipe_through :with_session

    get "/", BulletinController, :index

    # resources "/subscribers", SubscriberController
    resources "/users", UserController, only: [:show, :edit, :new, :create, :update]
    resources "/boards", BulletinController

    resources "/events", EventController, only: [:edit, :update, :delete]
    get "/boards/:bulletin_id/new_event", EventController, :new
    post "/boards/:bulletin_id/new_event", EventController, :create

    get "/boards/:bulletin_id/notifications", NotificationController, :index
    get "/boards/:bulletin_id/new_notification", NotificationController, :new
    post "/boards/:bulletin_id/new_notification", NotificationController, :create

    # resources "/notifications", NotificationController

    get "/signin/:token", SessionController, :show, as: :signin
    get "/signin", SessionController, :new
    post "/signin", SessionController, :create
    delete "/signout", SessionController, :delete
  end

  # Other scopes may use custom stacks.
  scope "/api", NotifyEventsWeb do
    pipe_through :browser_api

    post "/subscribers", SubscriberController, :create
    delete "/subscribers", SubscriberController, :delete
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: NotifyEventsWeb.Telemetry
    end
  end
end
