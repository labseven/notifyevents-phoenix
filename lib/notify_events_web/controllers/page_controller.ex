defmodule NotifyEventsWeb.PageController do
  use NotifyEventsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
