defmodule NotifyEventsWeb.BulletinController do
  use NotifyEventsWeb, :controller

  alias NotifyEvents.Board
  alias NotifyEvents.Board.Bulletin

  plug :authenticate_user when action not in [:index, :show]
  plug :authorize_user when action in [:edit, :update, :delete]

  def index(conn, _params) do
    bulletins = Board.list_bulletins()
    render(conn, "index.html", bulletins: bulletins)
  end

  def new(conn, _params) do
    changeset = Board.change_bulletin(%Bulletin{})
    render(conn, "new.html", changeset: changeset, user: conn.assigns[:current_user])
  end

  def create(conn, %{"bulletin" => bulletin_params}) do
    bulletin_params = Map.merge(bulletin_params, %{"user_id" => conn.assigns[:current_user].id})

    case Board.create_bulletin(bulletin_params) do
      {:ok, bulletin} ->
        conn
        |> put_flash(:info, "Bulletin created successfully.")
        |> redirect(to: Routes.bulletin_path(conn, :show, bulletin))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    bulletin = Board.get_bulletin!(id)
    events = Board.list_events_by_bulletin(id)
    conn = put_layout(conn, "bulletin.html")
    render(conn, "show_app.html", bulletin: bulletin, events: events)
  end

  def edit(conn, %{"id" => id}) do
    bulletin = Board.get_bulletin!(id)
    changeset = Board.change_bulletin(bulletin)
    render(conn, "edit.html", bulletin: bulletin, changeset: changeset)
  end

  def update(conn, %{"id" => id, "bulletin" => bulletin_params}) do
    bulletin = Board.get_bulletin!(id)

    bulletin_params = Map.merge(bulletin_params, %{"user_id" => conn.assigns[:current_user].id})

    case Board.update_bulletin(bulletin, bulletin_params) do
      {:ok, bulletin} ->
        conn
        |> put_flash(:info, "Bulletin updated successfully.")
        |> redirect(to: Routes.bulletin_path(conn, :show, bulletin))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", bulletin: bulletin, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    bulletin = Board.get_bulletin!(id)
    {:ok, _bulletin} = Board.delete_bulletin(bulletin)

    conn
    |> put_flash(:info, "Bulletin deleted successfully.")
    |> redirect(to: Routes.bulletin_path(conn, :index))
  end

  defp authenticate_user(conn, _params) do
    if conn.assigns.current_user != nil do
      conn
    else
      conn
      |> put_flash(:error, "You need to be signed in to do that!")
      |> redirect(to: Routes.session_path(conn, :new))
      |> halt()
    end
  end

  defp authorize_user(conn, _params) do
    %{params: %{"id" => bulletin_id}} = conn
    bulletin = Board.get_bulletin!(bulletin_id)

    if conn.assigns.current_user.id == bulletin.user_id do
      conn
    else
      conn
      |> put_flash(:error, "You are not the owner of this bulletin!")
      |> redirect(to: Routes.bulletin_path(conn, :show, bulletin_id))
      |> halt()
    end
  end
end
