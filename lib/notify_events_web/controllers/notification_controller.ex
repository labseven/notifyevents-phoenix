defmodule NotifyEventsWeb.NotificationController do
  use NotifyEventsWeb, :controller

  alias NotifyEvents.Board
  alias NotifyEvents.Board.Notification

  plug :authenticate_user
  plug :authorize_user

  def index(conn, %{"bulletin_id" => bulletin_id}) do
    notifications = Board.list_notifications_by_bulletin(bulletin_id)
    render(conn, "index.html", notifications: notifications, bulletin_id: bulletin_id)
  end

  def new(conn, %{"bulletin_id" => bulletin_id}) do
    changeset = Board.change_notification(%Notification{})
    bulletin = Board.get_bulletin!(bulletin_id)
    render(conn, "new.html", changeset: changeset, bulletin: bulletin)
  end

  def create(conn, %{"notification" => notification_params, "bulletin_id" => bulletin_id}) do
    notification_params = Map.merge(notification_params, %{"bulletin_id" => bulletin_id})

    case Board.create_notification(notification_params) do
      {:ok, notification} ->
        conn
        |> put_flash(:info, "Notification sent!")
        |> redirect(to: Routes.notification_path(conn, :index, bulletin_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    notification = Board.get_notification!(id)
    render(conn, "show.html", notification: notification)
  end

  def edit(conn, %{"id" => id}) do
    notification = Board.get_notification!(id)
    changeset = Board.change_notification(notification)
    render(conn, "edit.html", notification: notification, changeset: changeset)
  end

  def update(conn, %{"id" => id, "notification" => notification_params}) do
    notification = Board.get_notification!(id)

    case Board.update_notification(notification, notification_params) do
      {:ok, notification} ->
        conn
        |> put_flash(:info, "Notification updated successfully.")
        |> redirect(to: Routes.notification_path(conn, :show, notification))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", notification: notification, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    notification = Board.get_notification!(id)
    {:ok, _notification} = Board.delete_notification(notification)

    conn
    |> put_flash(:info, "Notification deleted successfully.")
    |> redirect(to: Routes.notification_path(conn, :index))
  end

  defp authenticate_user(conn, _params) do
    if conn.assigns.current_user != nil do
      conn
    else
      conn
      |> put_flash(:error, "You need to be signed in to do that!")
      |> redirect(to: Routes.session_path(conn, :new))
      |> halt()
    end
  end

  defp authorize_user(conn, _params) do
    %{params: %{"bulletin_id" => bulletin_id}} = conn
    bulletin = Board.get_bulletin!(bulletin_id)

    if conn.assigns.current_user.id == bulletin.user_id do
      conn
    else
      conn
      |> put_flash(:error, "You are not the owner of this bulletin!")
      |> redirect(to: Routes.bulletin_path(conn, :show, bulletin_id))
      |> halt()
    end
  end
end
