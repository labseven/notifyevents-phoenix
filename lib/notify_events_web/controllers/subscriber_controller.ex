defmodule NotifyEventsWeb.SubscriberController do
  use NotifyEventsWeb, :controller

  alias NotifyEvents.Board
  alias NotifyEvents.Board.Subscriber

  def create(conn, %{"subscriber" => subscriber_params}) do
    subscriber_params =
      Map.merge(subscriber_params, %{
        "ip_addr" => to_string(:inet_parse.ntoa(conn.remote_ip)),
        "user_agent" => get_req_header(conn, "user-agent") |> Enum.at(0)
      })

    case Board.create_subscriber(subscriber_params) do
      {:ok, _subsriber} ->
        conn
        |> send_resp(200, "")

      {:error, _changeset} ->
        conn
        |> send_resp(400, "")
    end
  end

  def delete(conn, %{"subscriber" => subscriber_params}) do
    subscriber =
      Board.get_subscriber_by_endpoint_bulletin(
        subscriber_params["endpoint"],
        subscriber_params["bulletin_id"]
      )

    if subscriber do
      Board.delete_subscriber(subscriber)
    end

    conn
    |> send_resp(200, "")
  end
end
