defmodule NotifyEventsWeb.EventController do
  use NotifyEventsWeb, :controller

  alias NotifyEvents.Board
  alias NotifyEvents.Board.Event

  plug :authenticate_user when action not in [:index, :show]
  plug :authorize_user when action not in [:index, :show]

  def index(conn, _params) do
    events = Board.list_events()
    render(conn, "index.html", events: events)
  end

  def new(conn, %{"bulletin_id" => bulletin_id}) do
    {:ok, now} = DateTime.now("America/Los_Angeles")
    now = DateTime.to_naive(now)
    bulletin = Board.get_bulletin!(bulletin_id)

    changeset =
      Board.change_event(%Event{
        start_date: now
      })

    render(conn, "new.html", changeset: changeset, bulletin_id: bulletin_id, bulletin: bulletin)
  end

  def create(conn, %{"event" => event_params, "bulletin_id" => bulletin_id}) do
    event_params = Map.merge(event_params, %{"bulletin_id" => bulletin_id})

    case Board.create_event(event_params) do
      {:ok, event} ->
        conn
        |> put_flash(:info, "Event created successfully.")
        |> redirect(to: Routes.bulletin_path(conn, :show, bulletin_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    event = Board.get_event!(id)
    render(conn, "show.html", event: event)
  end

  def edit(conn, %{"id" => id}) do
    event = Board.get_event!(id)
    bulletin_id = event.bulletin_id
    changeset = Board.change_event(event)
    render(conn, "edit.html", event: event, changeset: changeset, bulletin_id: bulletin_id)
  end

  def update(conn, %{"id" => id, "event" => event_params}) do
    event = Board.get_event!(id)

    case Board.update_event(event, event_params) do
      {:ok, event} ->
        conn
        |> put_flash(:info, "Event updated successfully.")
        |> redirect(to: Routes.bulletin_path(conn, :show, event.bulletin_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", event: event, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    event = Board.get_event!(id)
    {:ok, _event} = Board.delete_event(event)

    conn
    |> put_flash(:info, "Event deleted successfully.")
    |> redirect(to: Routes.bulletin_path(conn, :show, event.bulletin_id))
  end

  defp authenticate_user(conn, _params) do
    if conn.assigns.current_user != nil do
      conn
    else
      conn
      |> put_flash(:error, "You need to be signed in to do that!")
      |> redirect(to: Routes.session_path(conn, :new))
      |> halt()
    end
  end

  defp authorize_user(conn, _params) do
    bulletin_id =
      if conn.params["bulletin_id"] do
        conn.params["bulletin_id"]
      else
        %{params: %{"id" => event_id}} = conn
        Board.get_event!(event_id).bulletin_id
      end

    bulletin_user_id = Board.get_bulletin!(bulletin_id).user_id

    if conn.assigns.current_user.id == bulletin_user_id do
      conn
    else
      conn
      |> put_flash(:error, "You are not the owner of this bulletin!")
      |> redirect(to: Routes.bulletin_path(conn, :show, bulletin_id))
      |> halt()
    end
  end
end
