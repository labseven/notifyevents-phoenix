defmodule NotifyEvents.Board.Event do
  use Ecto.Schema
  import Ecto.Changeset

  alias NotifyEvents.Board

  schema "events" do
    field :body, :string
    field :start_date, :naive_datetime
    field :title, :string

    belongs_to :bulletin, Board.Bulletin

    timestamps()
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [:title, :body, :start_date, :bulletin_id])
    |> validate_required([:title, :body, :start_date])
  end
end
