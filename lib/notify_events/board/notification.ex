defmodule NotifyEvents.Board.Notification do
  use Ecto.Schema
  import Ecto.Changeset

  schema "notifications" do
    field :body, :string
    field :title, :string

    belongs_to :bulletin, Bulletin

    timestamps()
  end

  @doc false
  def changeset(notification, attrs) do
    notification
    |> cast(attrs, [:title, :body, :bulletin_id])
    |> validate_required([:title, :body, :bulletin_id])
  end
end
