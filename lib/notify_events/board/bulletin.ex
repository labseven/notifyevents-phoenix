defmodule NotifyEvents.Board.Bulletin do
  use Ecto.Schema
  import Ecto.Changeset

  alias NotifyEvents.Board

  schema "bulletins" do
    field :title, :string

    has_many :subscribers, Board.Subscriber
    has_many :events, Board.Event
    has_many :notifications, Board.Notification

    belongs_to :user, NotifyEvents.Accounts.User


    timestamps()
  end

  @doc false
  def changeset(bulletin, attrs) do
    bulletin
    |> cast(attrs, [:title, :user_id])
    |> validate_required([:title, :user_id])
  end
end
