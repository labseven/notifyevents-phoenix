defmodule NotifyEvents.Board.Subscriber do
  use Ecto.Schema
  import Ecto.Changeset

  schema "subscribers" do
    field :ip_addr, :string
    field :name, :string
    field :pn_auth, :string
    field :pn_endpoint, :string
    field :pn_p256dh, :string
    field :user_agent, :string

    belongs_to :bulletin, Bulletin

    timestamps()
  end

  @doc false
  def changeset(subscriber, attrs) do
    subscriber
    |> cast(attrs, [
      :name,
      :pn_endpoint,
      :pn_p256dh,
      :pn_auth,
      :ip_addr,
      :user_agent,
      :bulletin_id
    ])
    |> validate_required([:pn_endpoint, :pn_p256dh, :pn_auth, :ip_addr, :user_agent, :bulletin_id])
  end
end
