defmodule NotifyEvents.Board do
  @moduledoc """
  The Board context.
  """

  import Ecto.Query, warn: false
  alias NotifyEvents.Repo

  alias NotifyEvents.Board.Subscriber

  @doc """
  Returns the list of subscribers.

  ## Examples

      iex> list_subscribers()
      [%Subscriber{}, ...]

  """
  def list_subscribers do
    Repo.all(Subscriber)
  end

  def list_subscribers_by_bulletin(bulletin_id) do
    Repo.all(from s in Subscriber, where: s.bulletin_id == ^bulletin_id)
  end

  @doc """
  Gets a single subscriber.

  Raises `Ecto.NoResultsError` if the Subscriber does not exist.

  ## Examples

      iex> get_subscriber!(123)
      %Subscriber{}

      iex> get_subscriber!(456)
      ** (Ecto.NoResultsError)

  """
  def get_subscriber!(id), do: Repo.get!(Subscriber, id)

  @doc """
  Gets a single subscriber by endpoint.

  Raises `Ecto.NoResultsError` if the Subscriber does not exist.
  """
  def get_subscriber_by_endpoint_bulletin(endpoint, bulletin_id) do
    Subscriber
    |> where(pn_endpoint: ^endpoint)
    |> where(bulletin_id: ^bulletin_id)
    |> Repo.one()
  end

  @doc """
  Creates a subscriber.

  ## Examples

      iex> create_subscriber(%{field: value})
      {:ok, %Subscriber{}}

      iex> create_subscriber(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_subscriber(attrs \\ %{}) do
    %Subscriber{}
    |> Subscriber.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a subscriber.

  ## Examples

      iex> update_subscriber(subscriber, %{field: new_value})
      {:ok, %Subscriber{}}

      iex> update_subscriber(subscriber, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_subscriber(%Subscriber{} = subscriber, attrs) do
    subscriber
    |> Subscriber.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a subscriber.

  ## Examples

      iex> delete_subscriber(subscriber)
      {:ok, %Subscriber{}}

      iex> delete_subscriber(subscriber)
      {:error, %Ecto.Changeset{}}

  """
  def delete_subscriber(%Subscriber{} = subscriber) do
    Repo.delete(subscriber)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking subscriber changes.

  ## Examples

      iex> change_subscriber(subscriber)
      %Ecto.Changeset{data: %Subscriber{}}

  """
  def change_subscriber(%Subscriber{} = subscriber, attrs \\ %{}) do
    Subscriber.changeset(subscriber, attrs)
  end

  alias NotifyEvents.Board.Event

  @doc """
  Returns the list of events.

  ## Examples

      iex> list_events()
      [%Event{}, ...]

  """
  def list_events do
    Repo.all(Event)
  end

  def list_events_by_bulletin(bulletin_id) do
    {:ok, now} = DateTime.now("America/Los_Angeles")
    today = DateTime.to_date(now)

    Repo.all(
      from e in Event,
        where: e.bulletin_id == ^bulletin_id,
        where: fragment("?::date", e.start_date) >= ^today
    )
  end

  @doc """
  Gets a single event.

  Raises `Ecto.NoResultsError` if the Event does not exist.

  ## Examples

      iex> get_event!(123)
      %Event{}

      iex> get_event!(456)
      ** (Ecto.NoResultsError)

  """

  # def get_event!(id), do: Repo.get!(Event, id)

  def get_event!(id) do
    Repo.one!(
      from b in Event,
        where: b.id == ^id,
        preload: [:bulletin]
    )
  end

  @doc """
  Creates a event.

  ## Examples

      iex> create_event(%{field: value})
      {:ok, %Event{}}

      iex> create_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event(attrs \\ %{}) do
    %Event{}
    |> Event.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a event.

  ## Examples

      iex> update_event(event, %{field: new_value})
      {:ok, %Event{}}

      iex> update_event(event, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_event(%Event{} = event, attrs) do
    event
    |> Event.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a event.

  ## Examples

      iex> delete_event(event)
      {:ok, %Event{}}

      iex> delete_event(event)
      {:error, %Ecto.Changeset{}}

  """
  def delete_event(%Event{} = event) do
    Repo.delete(event)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking event changes.

  ## Examples

      iex> change_event(event)
      %Ecto.Changeset{data: %Event{}}

  """
  def change_event(%Event{} = event, attrs \\ %{}) do
    Event.changeset(event, attrs)
  end

  alias NotifyEvents.Board.Notification

  @doc """
  Returns the list of notifications.

  ## Examples

      iex> list_notifications()
      [%Notification{}, ...]

  """
  def list_notifications do
    Repo.all(Notification)
  end

  def list_notifications_by_bulletin(bulletin_id) do
    Repo.all(from n in Notification, where: n.bulletin_id == ^bulletin_id)
  end

  @doc """
  Gets a single notification.

  Raises `Ecto.NoResultsError` if the Notification does not exist.

  ## Examples

      iex> get_notification!(123)
      %Notification{}

      iex> get_notification!(456)
      ** (Ecto.NoResultsError)

  """
  def get_notification!(id), do: Repo.get!(Notification, id)

  @doc """
  Creates a notification.

  ## Examples

      iex> create_notification(%{field: value})
      {:ok, %Notification{}}

      iex> create_notification(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_notification(attrs \\ %{}) do
    %Notification{}
    |> Notification.changeset(attrs)
    |> Repo.insert()
    |> send_notifications()
  end

  defp send_notifications({:ok, notification}) do
    list_subscribers_by_bulletin(notification.bulletin_id)
    |> Enum.each(fn subscriber ->
      IO.inspect(subscriber)
      body = Jason.encode!(%{"title" => notification.title, "body" => notification.body})

      subscription = %{
        keys: %{p256dh: subscriber.pn_p256dh, auth: subscriber.pn_auth},
        endpoint: subscriber.pn_endpoint
      }

      {:ok, response} = WebPushEncryption.send_web_push(body, subscription)
      IO.inspect(response)
    end)

    {:ok, notification}
  end

  @doc """
  Updates a notification.

  ## Examples

      iex> update_notification(notification, %{field: new_value})
      {:ok, %Notification{}}

      iex> update_notification(notification, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_notification(%Notification{} = notification, attrs) do
    notification
    |> Notification.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a notification.

  ## Examples

      iex> delete_notification(notification)
      {:ok, %Notification{}}

      iex> delete_notification(notification)
      {:error, %Ecto.Changeset{}}

  """
  def delete_notification(%Notification{} = notification) do
    Repo.delete(notification)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking notification changes.

  ## Examples

      iex> change_notification(notification)
      %Ecto.Changeset{data: %Notification{}}

  """
  def change_notification(%Notification{} = notification, attrs \\ %{}) do
    Notification.changeset(notification, attrs)
  end

  alias NotifyEvents.Board.Bulletin

  @doc """
  Returns the list of bulletins.

  ## Examples

      iex> list_bulletins()
      [%Bulletin{}, ...]

  """
  def list_bulletins do
    Repo.all(from b in Bulletin, preload: [:user, :events, :subscribers])
  end

  @doc """
  Gets a single bulletin.

  Raises `Ecto.NoResultsError` if the Bulletin does not exist.

  ## Examples

      iex> get_bulletin!(123)
      %Bulletin{}

      iex> get_bulletin!(456)
      ** (Ecto.NoResultsError)

  """
  # def get_bulletin!(id), do: Repo.get!(Bulletin, id)
  # def get_bulletin!(id), do: Repo.one! from b in Bulletin,
  #                             where: b.id == ^id,
  #                             join: u in assoc(b, :user),
  #                             join: e in assoc(b, :events),
  #                             # join: s in assoc(b, :subscribers),
  #                             # select_merge: %{subscriber_count: count(s.id)},
  #                             # preload: [user: u, events: e, subscribers: s]
  #                             preload: [user: u, events: e]

  # TODO: Use SQL to load subscriber count
  def get_bulletin!(id) do
    Repo.one!(
      from b in Bulletin,
        where: b.id == ^id,
        preload: [:user, :subscribers]
    )
  end

  @doc """
  Creates a bulletin.

  ## Examples

      iex> create_bulletin(%{field: value})
      {:ok, %Bulletin{}}

      iex> create_bulletin(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_bulletin(attrs \\ %{}) do
    %Bulletin{}
    |> Bulletin.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a bulletin.

  ## Examples

      iex> update_bulletin(bulletin, %{field: new_value})
      {:ok, %Bulletin{}}

      iex> update_bulletin(bulletin, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_bulletin(%Bulletin{} = bulletin, attrs) do
    bulletin
    |> Bulletin.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a bulletin.

  ## Examples

      iex> delete_bulletin(bulletin)
      {:ok, %Bulletin{}}

      iex> delete_bulletin(bulletin)
      {:error, %Ecto.Changeset{}}

  """
  def delete_bulletin(%Bulletin{} = bulletin) do
    Repo.delete(bulletin)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking bulletin changes.

  ## Examples

      iex> change_bulletin(bulletin)
      %Ecto.Changeset{data: %Bulletin{}}

  """
  def change_bulletin(%Bulletin{} = bulletin, attrs \\ %{}) do
    Bulletin.changeset(bulletin, attrs)
  end
end
