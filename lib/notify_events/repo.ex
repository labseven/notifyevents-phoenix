defmodule NotifyEvents.Repo do
  use Ecto.Repo,
    otp_app: :notify_events,
    adapter: Ecto.Adapters.Postgres
end
